# ESLint Configs

My personal configs for various ESLint projects.

Simply use `install-peerdeps` to add a config to a project:

```
yarn global add install-peerdeps
install-peerdeps --dev --yarn eslint-config-travisreynolds-vue
```

Now create a `.eslintrc.js` file to specifiy the config:

```
module.exports = {
  "extends": ["travisreynolds-vue"]
}
```
