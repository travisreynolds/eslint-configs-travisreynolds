module.exports = {
  "env": {
    "es6": true,
    "browser": true,
    "node": true
  },
  "extends": [
    "standard",
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:jsx-a11y/recommended"
  ],
  "globals": {
    "Atomics": "readonly",
    "SharedArrayBuffer": "readonly",
    "graphql": true
  },
  "parser": "babel-eslint",
  "parserOptions": {
    "sourceType": "module",
    "ecmaVersion": 2018,
    "ecmaFeatures": {
      "jsx": true
    }
  },
  "plugins": [
    "html",
    "react",
    "react-hooks",
    "jsx-a11y"
  ],
  "settings": {
    "react": {
      "version": "detect"
    },
    "linkComponents": [
      "Hyperlink",
      {"name": "Link", "linkAttribute": "to"}
    ]
  },
  "rules": {
    "indent": [ "error", 2 ],
    "semi": [ "error", "never" ],
    "quotes": [
      2,
      "single",
      {
        "avoidEscape": true,
        "allowTemplateLiterals": true
      }
    ],
    "comma-dangle": [ "error", "never" ],
    "arrow-parens": [ "error", "as-needed" ],
    "linebreak-style": 0,
    "object-curly-spacing": [ "error", "always" ],
    "computed-property-spacing": [ "error", "always" ],
    "space-in-parens": [ "error", "never" ],
    "array-bracket-spacing": [ "error", "never" ],
    "camelcase": [ "off", {
      "properties": "never",
      "ignoreDestructuring": true
    } ],
    "prefer-const": [
      "error",
      {
        "destructuring": "all",
      }
    ],
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "warn",
    "jsx-a11y/anchor-is-valid": [
      "error",
      {
        "components": [ "Link" ],
        "specialLink": [ "to" ]
      }
    ]
  }
}
