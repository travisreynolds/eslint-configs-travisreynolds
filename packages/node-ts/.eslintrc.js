module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint'
  ],
  extends: [
    'standard',
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  rules: {
    indent: ['error', 2],
    semi: ['error', 'never'],
    quotes: [2, 'single', { avoidEscape: true, allowTemplateLiterals: true }],
    'comma-dangle': ['error', 'never'],
    'arrow-parens': ['error', 'as-needed'],
    'linebreak-style': 0,
    'object-curly-spacing': ['error', 'always'],
    'computed-property-spacing': ['error', 'always'],
    'space-in-parens': ['error', 'never'],
    'array-bracket-spacing': ['error', 'never'],
    camelcase: ['off', { properties: 'never', ignoreDestructuring: true }],
    'prefer-const': ['error', { destructuring: 'all' }]
  }
}
